package br.ucsal.bes20202.bd2.veiculo.domain;

public enum TipoVeiculoEnum {

	CARGA("CRG"),

	PASSEIO("PSS");

	private String sigla;

	private TipoVeiculoEnum(String sigla) {
		this.sigla = sigla;
	}

	public String getSigla() {
		return sigla;
	}

	public static TipoVeiculoEnum valueOfsigla(String sigla) {
		for (TipoVeiculoEnum situacao : values()) {
			if (situacao.getSigla().equalsIgnoreCase(sigla)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException("Sigla não encontrado: " + sigla);
	}

}
