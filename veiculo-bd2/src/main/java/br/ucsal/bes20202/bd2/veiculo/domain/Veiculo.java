package br.ucsal.bes20202.bd2.veiculo.domain;

import java.time.LocalDate;
import java.util.List;

public class Veiculo {

	private String placa;

	private String chassis;

	private Modelo modelo;

	private TipoVeiculoEnum tipo;

	private LocalDate dataCompra;

	private List<String> siglasAcessorios;

	public Veiculo() {
	}

	public Veiculo(String placa, String chassis, Modelo modelo, TipoVeiculoEnum tipo, LocalDate dataCompra,
			List<String> siglasAcessorios) {
		this();
		this.placa = placa;
		this.chassis = chassis;
		this.modelo = modelo;
		this.tipo = tipo;
		this.dataCompra = dataCompra;
		this.siglasAcessorios = siglasAcessorios;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	public Modelo getModelo() {
		return modelo;
	}

	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	public LocalDate getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(LocalDate dataCompra) {
		this.dataCompra = dataCompra;
	}

	public List<String> getSiglasAcessorios() {
		return siglasAcessorios;
	}

	public void setSiglasAcessorios(List<String> siglasAcessorios) {
		this.siglasAcessorios = siglasAcessorios;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((chassis == null) ? 0 : chassis.hashCode());
		result = prime * result + ((dataCompra == null) ? 0 : dataCompra.hashCode());
		result = prime * result + ((modelo == null) ? 0 : modelo.hashCode());
		result = prime * result + ((placa == null) ? 0 : placa.hashCode());
		result = prime * result + ((siglasAcessorios == null) ? 0 : siglasAcessorios.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Veiculo other = (Veiculo) obj;
		if (chassis == null) {
			if (other.chassis != null)
				return false;
		} else if (!chassis.equals(other.chassis))
			return false;
		if (dataCompra == null) {
			if (other.dataCompra != null)
				return false;
		} else if (!dataCompra.equals(other.dataCompra))
			return false;
		if (modelo == null) {
			if (other.modelo != null)
				return false;
		} else if (!modelo.equals(other.modelo))
			return false;
		if (placa == null) {
			if (other.placa != null)
				return false;
		} else if (!placa.equals(other.placa))
			return false;
		if (siglasAcessorios == null) {
			if (other.siglasAcessorios != null)
				return false;
		} else if (!siglasAcessorios.equals(other.siglasAcessorios))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", chassis=" + chassis + ", modelo=" + modelo + ", tipo=" + tipo
				+ ", dataCompra=" + dataCompra + ", siglasAcessorios=" + siglasAcessorios + "]";
	}

}